package marketing.mailshots;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;

@RunWith(ConcordionRunner.class)
public class SplittingNamesTest {

    public Map split(String fullName) {
        String[] words = fullName.split(" ");
        Map<String, String> result = new HashMap<>();
        result.put("firstName", words[0]);
        result.put("lastName", words[1]);
        return result;
    }
}